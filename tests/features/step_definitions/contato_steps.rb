Dado("que {string} é meu novo contato") do |nome|
  @nome = nome
end

Dado("este contato possui os seguintes dados:") do |table|
  @dados_contato = table.rows_hash
end

Quando("faço o cadastro deste novo contato") do
  visit 'http://127.0.0.1:8080/form-novo.html'
  #fill_in só funciona com elementos que tenham ID
  fill_in 'nome', with: @nome
  fill_in 'email', with: @dados_contato[:email]
  fill_in 'celular', with: @dados_contato[:celular]

  sleep 5
end

Então("devo ver a seguinte mensagem de sucesso {string}") do |mensagem|
  pending # Write code here that turns the phrase above into concrete actions
end