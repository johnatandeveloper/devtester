#language: pt

  Funcionalidade: Novo Contato
    Sendo um usuário
    Posso realizar um novo cadastro
    Para poder gerenciar minha rede de contatos

    @sprint1
    Esquema do Cenario: Novo Contato

      Dado que "<nome>" é meu novo contato
      E este contato possui os seguintes dados:
        |email  |<email>  |
        |celular|<celular>|
        |tipo   |<tipo>   |
      Quando faço o cadastro deste novo contato
      Então devo ver a seguinte mensagem de sucesso "Contato cadastrado com sucesso."

      Exemplos:
      |nome         |email          |celular    |tipo    |
      |Wade Wilson  |wade@marvel.com|61991222222|Whats   |
      |Tony Stark   |tonny@stark.com|61991222233|Telegram|
      |Steve Rogers |rogers@aol.com |61991222244|SMS     |
      |Bruce Barner |               |61991222255|SMS     |

      Esquema do Cenario: Campos obrigatórios

        Dado que "<nome>" é meu novo contato
        E este contato possui os seguintes dados:
          |celular|<celular>|
          |tipo   |<tipo>   |
        Quando faço o cadastro deste novo contato
        Então devo ver a seguinte "<mensagem>" de alerta

          Exemplos:
            |nome         |celular    |tipo      |mensagem                                   |
            |             |61991222222|Whats     |Ops. O nome deve ser preenchido            |
            |Peter Parker |           |Telegram  |Ops. O celucar deve ser preenchido         |
            |Scott Lang   |61991222222|          |Ops. Por favor selecione um tipo de contato|

